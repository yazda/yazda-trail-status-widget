define(['underscore', 'jquery', 'slick', 'moment',
      'text!templates/large.html', 'text!templates/small.html'],
    function(_, $, slick, moment, large, small) {

      var TrailStatus = function(params) {

        var dataUrl = (params.dataUrl || 'https://trail-status.yazdaapp.com/') + 'api/trails/' + params.id;
        var $el = $(params.el);
        var ss = document.styleSheets;
        var re = /.*trail-status([^/]+\.)?css/;
        var foundSS = false;

        // Find Stylesheets
        for(var i = 0, max = ss.length; i < max; i++) {
          var el = ss[i];

          if(el.href && el.href.match(re)) {
            foundSS = true;
            return;
          }
        }

        if(!foundSS) {
          var link = document.createElement("link");
          var slickLink = document.createElement("link");
          link.rel = slickLink.rel = "stylesheet";
          link.href = params.base_url + "/css/trail-status.css";
          slickLink.href = "https://cdn-trail-status.yazdaapp.com/css/slick.css";

          document.getElementsByTagName("head")[0].appendChild(link);
          document.getElementsByTagName("head")[0].appendChild(slickLink);
        }

        $.getJSON(dataUrl, function(data) {
          var images = _.shuffle(data.images);

          if(params.size === 'small') {
            $el.html(_.template(small, {
              id:      data._id,
              name:    data.name,
              comment: data.comment,
              status:  data.status
            }));
          } else {
            $el.html(_.template(large,
                {
                  id:          data._id,
                  name:        data.name,
                  city:        data.city,
                  lastUpdated: moment(data.lastUpdated).from(),
                  images:      images,
                  comment:     data.comment,
                  status:      data.status,
                  miles:       data.miles,
                  ascent:      data.ascent,
                  descent:     data.descent,
                  type:        data.type,
                  level:       data.level
                }));
            $el.find('#slider').slick({
              autoplay:      true,
              arrows:        false,
              dots:          false,
              autoplaySpeed: 4000
            });

            $el.find('.trail-info-wrapper').addClass("fixed").hide();

            $el.find('.trail-info-button').show();

            $el.find('.trail-info-button').click(function(event) {
              event.stopPropagation();
              $el.find('.trail-info-wrapper').fadeToggle("fast");
              $el.find('.trail-info-button').toggleClass("fixed");
            });

            $el.click(function() {
              $el.find('.trail-info-wrapper').fadeOut("fast");
              $el.find('.trail-info-button').removeClass("fixed");
            });
          }
        });
      };

      return TrailStatus;

    });
